# はじめに

## この文書は何か
本文書は、筆者が [山形県立産業技術短期大学校庄内校](https://www.shonai-cit.ac.jp) で非常勤講師を担当している「情報工学」という科目で用いる教科書として執筆されたものです。
授業を受講されている学生さん向けに執筆された文書ではありますが、学生さんだけでなく広く一般の方々に役に立つ情報になると思い、Web上に一般公開する次第です。
ぜひ本文書を基に、情報工学を広く一般の方々にお伝えできれば幸いです。

なお、本文書のソースコードは [GitLab](https://gitlab.com/jo7ueb/ict4c) にて公開されております。
詳細は後述しますが、本文書をより良いものとするために、皆様からのフィードバックをぜひとも期待しております。

## 筆者について
筆者は、山形県酒田市で [ダーディット株式会社](https://dahdit.tech) という人工知能を研究するベンチャー企業を経営しているものです。
ちょっとしたご縁があり、産技短で情報工学を教えることとなりました。
筆者についてのプロフィールは [筆者Webサイト](https://yendo.info) をご覧ください。

## 取り扱うテーマ
本文書は「情報工学」の科目シラバスに従って、現代社会に欠かせない存在である情報通信技術(ICT)に関する様々な基本的なテーマを、広く浅く(といっても浅すぎずほどほどに)紹介しています。
単に知識を羅列するだけではなく、その根底にある情報工学的なものの考え方をお伝えできればと思って執筆しております。

本文書で取り扱うテーマは、主に以下のとおりです。

* そもそもなぜ情報工学を学ぶのか
* 情報通信技術の歴史
* 情報通信技術の現在
    * 情報通信技術を支えるハードウェア
    * 情報通信技術を支えるソフトウェア
    * 情報通信技術を支えるネットワーク
* 情報通信技術の未来

## 読者の方にお願いしたいこと
本文書は、著者の執筆時点で知りうる限りのベストを尽くして正しいと信ずる内容を執筆しております。
しかし筆者はまだまだ若輩者故、内容に不足があったり間違いがある可能性があります。
ぜひその際は [GitLab にて報告](https://gitlab.com/jo7ueb/ict4c/-/issues/new) をお願いいたします。
内容を確認したうえで、可能な限り対応させていただきます。
可能であれば [マージリクエスト](https://gitlab.com/jo7ueb/ict4c/-/merge_requests/new) を立てていただけますと、よりスムーズです。
ご協力をよろしくお願いいたします。

## 更新履歴
<div id="history-area">
</div>
<script async>
const glurl = "https://gitlab.com/api/v4/projects/35480852/repository/commits";
const area = document.getElementById("history-area");
fetch(glurl)
    .then(resp => resp.json())
    .then(data => data.slice(0, 10).forEach(record => {
        const commit_date = new Date(record.authored_date);
        const formatted_date = commit_date.getFullYear() + "/" + (commit_date.getMonth() + 1) + "/" + commit_date.getDate();
        area.innerHTML += "<p>" + formatted_date + ": <a href=\"" + record.web_url + "\">" + record.title + "</a></p>";
}));
</script>